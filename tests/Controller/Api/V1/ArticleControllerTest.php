<?php

declare(strict_types=1);

namespace App\Tests\Controller\Api\V1;

use Symfony\Component\HttpFoundation\Request;

class ArticleControllerTest extends WebTestCase
{
    public function testCRUD(): void
    {
        $title = 'Some test title';
        $body = 'Some test body';
        $this->client->request(Request::METHOD_POST, '/api/v1/articles', [], [], $this->getAuthHeader(), \json_encode([
            'title' => $title,
            'body' => $body,
        ]));
        $response = $this->client->getResponse();
        $responseBody = \json_decode($response->getContent(), true);
        self::assertArrayHasKey('data', $responseBody);
        self::assertSame($title, $responseBody['data']['title']);
        self::assertSame($body, $responseBody['data']['body']);

        $id = $responseBody['data']['id'];
        $title = 'Some new test title';
        $body = 'Some new test body';
        $this->client->request(Request::METHOD_PUT, "/api/v1/articles/{$id}", [], [], $this->getAuthHeader(), \json_encode([
            'title' => $title,
            'body' => $body,
        ]));
        $response = $this->client->getResponse();
        $responseBody = \json_decode($response->getContent(), true);
        self::assertArrayHasKey('data', $responseBody);
        self::assertSame($title, $responseBody['data']['title']);
        self::assertSame($body, $responseBody['data']['body']);

        $this->client->request(Request::METHOD_DELETE, "/api/v1/articles/{$id}", [], [], $this->getAuthHeader());
        $response = $this->client->getResponse();
        $responseBody = \json_decode($response->getContent(), true);
        self::assertArrayHasKey('success', $responseBody);
        self::assertTrue($responseBody['success']);
    }

    public function testList(): void
    {
        $range = range(1, 40);
        $lastId = null;
        foreach ($range as $id) {
            $testTitle = "Title #{$id}";
            $testBody = "Body #{$id}";
            $this->client->request(Request::METHOD_POST, '/api/v1/articles', [], [], $this->getAuthHeader(), \json_encode([
                'title' => $testTitle,
                'body' => $testBody,
            ]));
            $response = $this->client->getResponse();
            $responseBody = \json_decode($response->getContent(), true);
            $lastId = $responseBody['data']['id'];
        }

        $this->client->request(Request::METHOD_GET, '/api/v1/articles?per-page=5', [], [], $this->getAuthHeader());
        $response = $this->client->getResponse();
        $responseBody = \json_decode($response->getContent(), true);
        self::assertCount(5, $responseBody['data']);
        self::assertSame($lastId, $responseBody['data'][0]['id']);

        $this->client->request(Request::METHOD_GET, '/api/v1/articles?per-page=5&page=2', [], [], $this->getAuthHeader());
        $response = $this->client->getResponse();
        $responseBody = \json_decode($response->getContent(), true);
        self::assertCount(5, $responseBody['data']);
        self::assertNotSame($lastId, $responseBody['data'][0]['id']);

        $this->client->request(Request::METHOD_GET, '/api/v1/articles?sort-direction=asc', [], [], $this->getAuthHeader());
        $response = $this->client->getResponse();
        $responseBody = \json_decode($response->getContent(), true);
        self::assertNotSame($lastId, $responseBody['data'][0]['id']);
    }
}
