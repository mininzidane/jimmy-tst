<?php

declare(strict_types=1);

namespace App\Tests\Controller\Api\V1;

class WebTestCase extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase
{
    private const SECURE_TOKEN = '123456_SUPER_TOKEN';

    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();
    }

    protected function getAuthHeader(): array
    {
        return ['HTTP_X_AUTH_TOKEN' => self::SECURE_TOKEN];
    }
}
