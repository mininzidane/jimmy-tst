## Initial steps

1. Run `docker-compose up`
    - API is available by http://localhost:8013/
    - run `docker-compose exec php composer i`
    - you can connect to db by credentials in .env file (outside container: 127.0.0.1:3308)
    - create a database using console command `docker-compose exec php bin/console doctrine:database:create`;
2. Run migrations `docker-compose exec php bin/console doctrine:migrations:migrate`
3. Requests must be sent with header `X-AUTH-TOKEN=123456_SUPER_TOKEN`.
4. There are tests for end-points. Execute by: `docker-compose exec php bin/phpunit tests/Controller/Api/V1/`
5. Documentation here: http://localhost:8013/api/doc