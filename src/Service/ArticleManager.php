<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;

class ArticleManager
{
    private EntityManagerInterface $entityManager;
    private ArticleRepository $articleRepository;

    public function __construct(EntityManagerInterface $entityManager, ArticleRepository $articleRepository)
    {
        $this->entityManager = $entityManager;
        $this->articleRepository = $articleRepository;
    }

    /**
     * @return Article[]
     */
    public function getList(string $sortBy, string $sortDirection, int $page, int $perPage): array
    {
        $limit = $perPage;
        $offset = ($page - 1) * $perPage;
        try {
            $articles = $this->articleRepository->getListSortedPaginated($sortBy, $sortDirection, $limit, $offset);
        } catch (\Throwable $e) {
            return [];
        }

        return $articles;
    }

    public function createArticle(array $data): Article
    {
        $article = new Article();
        $this->saveArticle($article, $data);

        return $article;
    }

    public function updateArticle(Article $article, array $data): void
    {
        $this->saveArticle($article, $data);
    }

    public function deleteArticle(Article $article): void
    {
        $this->entityManager->remove($article);
        $this->entityManager->flush();
    }

    private function saveArticle(Article $article, array $data): void
    {
        // todo add validation
        $article->setTitle($data['title'] ?? '');
        $article->setBody($data['body'] ?? null);
        $article->setUpdatedAt(new \DateTime());

        $this->entityManager->persist($article);
        $this->entityManager->flush();
    }
}
