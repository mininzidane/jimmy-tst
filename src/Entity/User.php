<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    public const ROLE_USER = 'ROLE_USER';

    public function getUsername(): ?string
    {
        return 'Mock Name';
    }

    public function getPassword(): ?string
    {
        return 'Mock Pass';
    }

    public function getRoles(): array
    {
        return [self::ROLE_USER];
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }
}
