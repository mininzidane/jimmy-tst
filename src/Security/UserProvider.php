<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {

    }

    public function loadUserByUsername(string $username)
    {
        return new User();
    }

    public function refreshUser(UserInterface $user)
    {
        return new User();
    }

    public function supportsClass(string $class)
    {
        return $class === User::class;
    }

}
