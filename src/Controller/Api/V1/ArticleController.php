<?php

declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Entity\Article;
use App\Service\ArticleManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model as OAModel;

class ArticleController extends AbstractController
{
    /**
     * @Route("/articles", name="api_v1_article_list", methods={"GET"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns article list",
     *     @OA\JsonContent(
     *         type="object",
     *         @OA\Property(property="data", type="array",
     *             @OA\Items(ref=@OAModel(type=Article::class, groups={"article_details"}))
     *         ),
     *     )
     * )
     * @OA\Parameter(
     *     name="sort-by",
     *     in="query",
     *     description="The field used to order articles",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="sort-direction",
     *     in="query",
     *     description="The field used to order articles direction",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="per-page",
     *     in="query",
     *     description="Items count on page",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     description="Page number",
     *     @OA\Schema(type="integer")
     * )
     * @OA\Tag(name="articles")
     */
    public function index(Request $request, ArticleManager $articleManager, NormalizerInterface $normalizer): JsonResponse
    {
        $sortBy = $request->get('sort-by', 'id');
        $sortDirection = $request->get('sort-direction', 'DESC');
        $perPage = (int)$request->get('per-page', 20);
        $page = (int)$request->get('page', 1);

        $articles = $articleManager->getList($sortBy, $sortDirection, $page, $perPage);

        return $this->successResponse([
            'data' => $normalizer->normalize($articles, 'json', ['groups' => 'article_details']),
        ]);
    }

    /**
     * @Route("/articles", name="api_v1_article_create", methods={"POST"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns created article",
     *     @OA\Schema(
     *         type="object",
     *         @OA\Property(
     *             property="data",
     *             type="object",
     *             ref=@OAModel(type=Article::class, groups={"article_details"})
     *         )
     *     )
     * )
     * @OA\Tag(name="articles")
     */
    public function create(Request $request, ArticleManager $articleManager, NormalizerInterface $normalizer): JsonResponse
    {
        $data = $request->request->all();

        try {
            $article = $articleManager->createArticle($data);
        } catch (\Throwable $e) {
            return $this->errorResponse(['error' => $e->getMessage()]);
        }

        return $this->successResponse([
            'data' => $normalizer->normalize($article, 'json', ['groups' => 'article_details']),
        ]);
    }

    /**
     * @Route("/articles/{id}", name="api_v1_article_update", methods={"PUT"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns updated article",
     *     @OA\Schema(
     *         type="object",
     *         @OA\Property(
     *             property="data",
     *             type="object",
     *             ref=@OAModel(type=Article::class, groups={"article_details"})
     *         )
     *     )
     * )
     * @OA\Tag(name="articles")
     */
    public function update(
        Request $request,
        Article $article,
        ArticleManager $articleManager,
        NormalizerInterface $normalizer
    ): JsonResponse {
        $data = $request->request->all();

        try {
            $articleManager->updateArticle($article, $data);
        } catch (\Throwable $e) {
            return $this->errorResponse(['error' => $e->getMessage()]);
        }

        return $this->successResponse([
            'data' => $normalizer->normalize($article, 'json', ['groups' => 'article_details']),
        ]);
    }

    /**
     * @Route("/articles/{id}", name="api_v1_article_delete", methods={"DELETE"})
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns true if successful deletion",
     *     @OA\Schema(
     *         type="object",
     *         @OA\Property(
     *             property="success",
     *             type="boolean"
     *         )
     *     )
     * )
     * @OA\Tag(name="articles")
     */
    public function delete(Article $article, ArticleManager $articleManager): JsonResponse
    {
        try {
            $articleManager->deleteArticle($article);
        } catch (\Throwable $e) {
            return $this->errorResponse(['error' => $e->getMessage()]);
        }

        return $this->successResponse(['success' => true]);
    }
}
